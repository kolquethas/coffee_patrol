1. Мой проект представляет из себя веб-приложение для поиска и выбора кофейни которая будет вам по душе.

2. Роли в системе:
    1. Гость(незарегестрированный пользователь) - возможность просмотра основного интерфейса(карта с отмеченными на ней кофейнями), просмотра профиля кофейни(название, описание, фото, отзывы), создание личного кабинета.
    2. Юзер/Пользователь - возможность просмотра основного интерфейса, просмотра профиля кофейни, добавления фото и отзывов о кофейне, просмотра своего личного кабинета и личного кабинета другого пользователя, и списка оставленных отзывов.
    3. Модератор - доступ к личным данным пользователей при регистрации, доступ к отзывам, возможность публикации/удаления оставленных юзером отзывов.
    4. Администратор - доступ к личным данным пользователей при регистрации, доступ к отзывам, возможность публицации/удаления оставленных юзером отзывов, доступ к списку зарегестрированных пользователей, возможность изменения назначения ролей и активации/деактивации аккаунтов.

3. Аналоги:
    1. Шаверма Патурль - https://play.google.com/store/apps/details?id=com.shaverma.patrol&hl=ru&gl=US - наличие общего рейтинга, карта с отмеченными точками, личный кабинет, меню с акциями и скидками (добавляются самими ресторанами), фотографии и отзывы.
    2. Google Maps - https://www.google.ru/maps - карта с возможностью поиска любых ресторанов, фотографии отзывы и краткое описание ресторана, личный кабинет google.
    3. Yandex Maps - https://yandex.ru/maps/2/saint-petersburg/?ll=30.315635%2C59.938951&z=11 - карта с возможностью поиска любых ресторанов, фотографии отзывы и краткое описание ресторана, личный кабинет yandex.

    3 - удобный интерфейс, 2 - есть недостатки, 1 - самый минимум, 0 - отсутствует

    | Название | Отзывы | Карта | Личный кабинет | Навигация между интерфейсами |
    | ------ | ------ | ------ | ------ | ------ |
    | Шаверма Патруль | 2      | 1 | 2 | 3 |
    | Google Maps     | 3 | 2 | 2 | 2 |
    | Yandex Maps     | 3 | 3 | 2 | 2 |  

4. Исходя из проведенного анализа аналагов можно выделить следующее: необходима удобная система отзывов и рейтингов, сортировка и система фильтров, интуитивно понятная навигация по интерфейсам приложения, личный кабинет, в котором можно отслеживать оставленные отзывы, удобная интерактивная карта с отмеченными кофейнями.

